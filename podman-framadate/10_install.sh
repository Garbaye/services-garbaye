#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_pod_not_exists ${pod_name}
ensure_variables_are_defined "$envvars"

if ! podman volume exists ${dbvolume} ; then
    echo "Error : conf volume ${dbvolume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

cat <<EOT >> .env
MYSQL_ROOT_PASSWORD=${GARBAYE_FRAMADATE_MYSQL_ROOT_PASSWORD}
MYSQL_PASSWORD=${GARBAYE_FRAMADATE_MYSQL_PASSWORD}
APP_NAME=${GARBAYE_FRAMADATE_APP_NAME}
DOMAIN=${GARBAYE_FRAMADATE_DOMAIN}
ADMIN_MAIL=${GARBAYE_FRAMADATE_ADMIN_MAIL}
ADMIN_PASSWORD=${GARBAYE_FRAMADATE_ADMIN_PASSWORD}
SMTP_SERVER=${GARBAYE_FRAMADATE_SMTP_SERVER}

EOT

export framadate_image
export framadate_version
export mysql_image
export mysql_version
export listen_if
export listen_port

if ! podman image exists ${framadate_image}:${framadate_version}; then
    podman image pull ${framadate_image}:${framadate_version}
fi
podman image pull ${mysql_image}:${mysql_version} &&

${my_podman_compose} --pod-args="--infra=true --infra-name=${project_name}_infra --share=" --podman-run-args "--requires=${project_name}_infra --env-file .env" up -d &&
echo -n "Waiting for initialization to end... " &&
( podman container logs -f framadate-app 2>&1 & ) | grep -q 'configured -- resuming normal operations' &&
echo "OK" &&
shred -u .env &&
podman pod stop ${pod_name}
