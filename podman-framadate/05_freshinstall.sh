#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_variables_are_defined "$envvars"

if podman volume exists ${dbvolume} ; then
        echo "Volume ${dbvolume} from previous installation already exists"
        echo "Please remove them before fresh install, or try to continue with normal installation"
        exit 1
fi

podman volume create ${dbvolume}

${ABSDIR}/10_install.sh
