FROM docker.io/library/php:8-apache

ARG version

RUN apt-get -y update \
 && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y -qq zip unzip git zlib1g-dev libicu-dev g++ default-mysql-client git locales \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && sed -i '/fr_FR.UTF-8/s/^# //g' /etc/locale.gen \
 && locale-gen \
 && docker-php-ext-install intl \
 && docker-php-ext-install pdo_mysql \
 && a2enmod rewrite

COPY --from=docker.io/library/composer:2.2 /usr/bin/composer /usr/bin/composer

COPY php.ini /usr/local/etc/php/php.ini
COPY apache-framadate.conf /etc/apache2/sites-enabled/framadate.conf
COPY entrypoint.sh /usr/local/bin/entrypoint

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN set -eux; \
    composer clear-cache
ENV PATH="${PATH}:/root/.composer/vendor/bin"
ENV COMPOSER_ALLOW_SUPERUSER 0

WORKDIR /var/www/framadate

RUN git clone -b $version --depth=1 https://framagit.org/framasoft/framadate/framadate.git . \
 && sed -i -e 's|"smarty/smarty": "^4.0",|"smarty/smarty": "^4.3",|' /var/www/framadate/composer.json \
 && sed -i -e 's|isset($admin)|isset($admin) \&\& $admin|g' /var/www/framadate/tpl/part/comments_list.tpl \
 && chown -R 33:33 . \
 && if [ "$ENV" = "dev" ] ; then echo Using PHP production mode ; else echo Using PHP development mode && echo "error_reporting = E_ERROR | E_WARNING | E_PARSE\ndisplay_errors = On" > /usr/local/etc/php/conf.d/php.ini ; fi \
 && rm /etc/apache2/sites-enabled/000-default.conf \
 && composer install -o --no-interaction --prefer-dist --no-dev \
 && composer dump-autoload --optimize --no-dev --classmap-authoritative \
 && composer update --no-interaction --no-dev

EXPOSE 80
ENTRYPOINT ["entrypoint"]
