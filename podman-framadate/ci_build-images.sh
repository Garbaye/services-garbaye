#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

if ! podman image exists ${framadate_image}:${framadate_version}; then
    TMPDIR=${HOME} podman image build \
	   -t "${framadate_image}":"${framadate_version}" \
	   --build-arg=version="${framadate_version}" \
	   container/ || retval=false
    podman image prune -a -f --filter dangling=true
    podman image prune -a -f --filter intermediate=true
    podman image rm -f $(podman image list -a -q -- docker.io/library/php)
    podman image rm -f $(podman image list -a -q -- docker.io/library/composer)
    eval "$retval"
else
    echo "Image ${framadate_image}:${framadate_version} already built"
fi &&

oci_push_to_registry ${framadate_image}:${framadate_version}
