#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_variables_are_defined "$envvars"

for volume in ${dbvolume} ${wpvolume}; do
    if podman volume exists ${volume} ; then
        echo "Volume ${volume} from previous installation already exists"
        echo "Please remove them before fresh install, or try to continue with normal installation"
        exit 1
    fi
done

podman volume create ${wpvolume} && \
podman volume create ${dbvolume}

${ABSDIR}/10_install.sh
