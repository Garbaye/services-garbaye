#!/usr/bin/env bash
## vars
wp_image='git.garbaye.fr/garbaye/wordpress'
wp_version='6.7.2-php8.2-apache'
mysql_image='docker.io/library/mariadb'
mysql_version='10.11'
## default vars : override with ENV var
listen_if="${GARBAYE_WPAMAPSTA_ENV_LISTENIF:-127.0.0.1}"
listen_port="${GARBAYE_WPAMAPSTA_ENV_LISTENPORT:-8092}"
## mandatory ENV vars
envvars='GARBAYE_WPAMAPSTA_MYSQL_PASSWORD'
## internal vars : do not touch
project_name=${PWD##*/}
pod_name="pod_${project_name}"
service_name="pod-${pod_name}.service"
upstream_images="${wp_image} docker.io/library/php"
dbvolume='podman-wpamapsta_db'
wpvolume='podman-wpamapsta_wp'
