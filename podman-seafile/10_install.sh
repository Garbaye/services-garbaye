#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_pod_not_exists ${pod_name}
ensure_variables_are_defined "$envvars"

if ! podman volume exists ${dbvolume} ; then
    echo "Error : DB volume ${dbvolume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

if ! podman volume exists ${datavolume} ; then
    echo "Error : DATA volume ${datavolume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

cat <<EOT >> .env
MYSQL_ROOT_PASSWORD=${GARBAYE_SEAFILE_MYSQL_ROOT_PASSWORD}
DB_ROOT_PASSWD=${GARBAYE_SEAFILE_MYSQL_ROOT_PASSWORD}
SEAFILE_ADMIN_PASSWORD=${GARBAYE_SEAFILE_ADMIN_PASSWORD}
SEAFILE_SERVER_HOSTNAME=${GARBAYE_SEAFILE_SERVER_HOSTNAME}
SEAFILE_ADMIN_EMAIL=${GARBAYE_SEAFILE_ADMIN_EMAIL}
EOT

export seafile_image
export seafile_release
export mariadb_release
export memcached_release
export listen_if
export listen_port
export listen_ws_port

if ! podman image exists ${seafile_image}:${seafile_release}; then
    podman image pull ${seafile_image}:${seafile_release} || exit 1
fi
podman image pull docker.io/library/memcached:${memcached_release} &&
podman image pull docker.io/library/mariadb:${mariadb_release} &&
${my_podman_compose} --pod-args="--infra=true --infra-name=${project_name}_infra --share=" --podman-run-args "--requires=${project_name}_infra --env-file .env" up -d &&
echo -n "Waiting for seahub_settings.py " &&
while [ ! -e `get_podman_volume_path ${datavolume}`/seafile/conf/seahub_settings.py ]; do
    echo -n "."
    sleep 1
done && echo "OK" &&
if ! grep -qF EMAIL_HOST `get_podman_volume_path ${datavolume}`/seafile/conf/seahub_settings.py; then
    echo 'Configuring seahub_settings.py'
    podman_unshare_sed_in_place "^FILE_SERVER_ROOT = \"http://${GARBAYE_SEAFILE_SERVER_HOSTNAME}/seafhttp\"" "FILE_SERVER_ROOT = \"https://${GARBAYE_SEAFILE_SERVER_HOSTNAME}/seafhttp\"\nCSRF_TRUSTED_ORIGINS = [\"https://${GARBAYE_SEAFILE_SERVER_HOSTNAME}\"]\nEMAIL_USE_TLS = False\nEMAIL_HOST = '${GARBAYE_SEAFILE_SMTP_SERVER}'\nEMAIL_HOST_USER = ''\nEMAIL_HOST_PASSWORD = ''\nEMAIL_PORT = 25\nDEFAULT_FROM_EMAIL = 'noreply-seafile@garbaye.fr'\nSERVER_EMAIL = 'noreply-seafile@garbaye.fr'" `get_podman_volume_path ${datavolume}`/seafile/conf/seahub_settings.py
else
    echo 'Configuration file seahub_settings.py already exists - not configuring.'
fi &&
if ! grep -Pzq "\[notification\]\nenabled = true\nhost = 0.0.0.0" `get_podman_volume_path ${datavolume}`/seafile/conf/seafile.conf; then
    echo 'Configuring seafile.conf'
    podman_unshare_sed_in_place_multiline "\[notification\]\nenabled = false\nhost = 127.0.0.1" "[notification]\nenabled = true\nhost = 0.0.0.0" `get_podman_volume_path ${datavolume}`/seafile/conf/seafile.conf
else
    echo 'Configuration file seafile.conf already exists - not configuring.'
fi &&
echo -n "Waiting for seahub to finish starting " &&
( podman container logs -f seafile 2>&1 & ) | grep -q 'Seahub is started' &&
echo "OK" &&
podman pod stop ${pod_name} &&
echo Pod built and stopped. &&
shred -u .env
