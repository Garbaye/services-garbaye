# Seafile
## Première installation  
### soucis d'URL  
À la première connexion en admin, changer les URL (https) dans l'interface d'administration.

Administrateur systeme
SERVICE_URL

## MAJ seafile 
### Stratégie
Pour les montées en version majeures, Seafile expérimente avec la version CE et résoud les soucis avant de publier la version EE. La version cible recommandée est celle qui est publiée en même temps que la EE.
### Procédure
Modifier la version cible dans `vars.sh` (ici pour l'exemple : `8.0.8`)
```
cd /opt/services-garbaye/podman-seafile
export GARBAYE_SEAFILE_MYSQL_ROOT_PASSWORD=XXXXXXXXXXXXXXXX
./40_stop.sh
./70_disable.sh
./80_destroy.sh
./10_install.sh
./20_enable.sh
./30_start.sh
```
## Restauration
À partir d'une copie des home utilisateur (volumes podman compris) dans `/backup` :
* En tant qu'utilisateur `podman-seafile` :
```
podman volume create podman-seafile_seafile-data
podman volume create podman-seafile_seafile-db
```
* En tant que `root` :
```
# La restauration par "mv" nécessite une correction du contexte SELinux. Privilégier "rsync" si possible.
mv /backup/home/podman-seafile/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data/* ~podman-seafile/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data/
mv /backup/home/podman-seafile/.local/share/containers/storage/volumes/podman-seafile_seafile-db/_data/* ~podman-seafile/.local/share/containers/storage/volumes/podman-seafile_seafile-db/_data/

# Initialiser les permissions à la racine des volumes
chown -R podman-seafile:podman-users ~podman-seafile/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data
chown -R podman-seafile:podman-users ~podman-seafile/.local/share/containers/storage/volumes/podman-seafile_seafile-db/_data

```
* En tant qu'utilisateur `podman-seafile` :
```
podman unshare chown -R 999:999 ~/.local/share/containers/storage/volumes/podman-seafile_seafile-db/_data
chown -R podman-seafile:podman-users /home/podman-seafile/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data/logs/var-log

podman unshare chown -R 0:43 ~/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data/logs/var-log/btmp*
podman unshare chown -R 0:43 ~/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data/logs/var-log/wtmp*
podman unshare chown -R 0:43 ~/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data/logs/var-log/lastlog
podman unshare chown -R 0:4 ~/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data/logs/var-log/apt/term.log*
podman unshare chown 0:4 ~/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data/logs/var-log/nginx
podman unshare chown 33:4 ~/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data/logs/var-log/nginx/*
podman unshare restorecon -R ~/.local/share/containers/storage/volumes/podman-seafile_seafile-db/_data
podman unshare restorecon -R ~/.local/share/containers/storage/volumes/podman-seafile_seafile-data/_data
```
* Exporter les variables d'environnement et procéder à l'installation normale (10_install.sh)
