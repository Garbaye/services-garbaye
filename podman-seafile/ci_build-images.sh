#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

buildfolder=/tmp/seafile-$$

if ! podman image exists ${seafile_image}:${seafile_release}; then
    mkdir ${buildfolder} &&
    if git clone --depth=1 https://github.com/haiwen/seafile-docker.git ${buildfolder}/ ; then
	sed_in_place "^FROM ubuntu" "FROM docker.io/ubuntu" ${buildfolder}/image/seafile_${seafile_release%.*}/Dockerfile &&
	sed_in_place "SEAFILE_VERSION=" "SEAFILE_VERSION=${seafile_release}" ${buildfolder}/image/seafile_${seafile_release%.*}/Dockerfile &&
	sed_in_place "pip -i https://pypi.tuna.tsinghua.edu.cn/simple/" "pip" ${buildfolder}/image/seafile_${seafile_release%.*}/Dockerfile &&
        sed_in_place "    -i https://pypi.tuna.tsinghua.edu.cn/simple/" "    " ${buildfolder}/image/seafile_${seafile_release%.*}/Dockerfile &&
        sed_in_place 'wget https://seafile-downloads.oss-cn-shanghai.aliyuncs.com' 'wget -q https://download.seadrive.org' ${buildfolder}/image/seafile_${seafile_release%.*}/Dockerfile &&
        sed_in_place 'tar -zxvf' 'tar -zxf' ${buildfolder}/image/seafile_${seafile_release%.*}/Dockerfile &&
        sed_in_place 'COPY scripts_11.0' 'COPY scripts/scripts_11.0' ${buildfolder}/image/seafile_${seafile_release%.*}/Dockerfile &&
        TMPDIR=${HOME} podman image build \
	       -t ${seafile_image}:${seafile_release} \
	       -f ${buildfolder}/image/seafile_${seafile_release%.*}/Dockerfile \
	       ${buildfolder} || retval=false
	podman image prune -a -f --filter dangling=true
	podman image prune -a -f --filter intermediate=true
	podman image rm -f $(podman image list -a -q -- ubuntu)
    fi
    rm -rf ${buildfolder}
    eval "$retval"
else
    echo "Image ${seafile_image}:${seafile_release} already built"
fi &&

oci_push_to_registry ${seafile_image}:${seafile_release}
