#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

# FAIL if pod does not exists.
ensure_pod_exists ${pod_name}

# FAIL if systemd unit does not exists.
ensure_systemd_unit_exists ${service_name}

# FAIL if systemd unit is running.
ensure_systemd_unit_not_running ${service_name}

# FAIL if pod is already running - without systemd control.
ensure_pod_not_running ${pod_name}

# OK
echo "Starting pod through systemd"
systemctl --user start ${service_name}
