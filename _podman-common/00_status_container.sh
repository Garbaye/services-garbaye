#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

echo "Podman images status..."
for image in ${upstream_images} ; do
    podman image list ${image} | grep ${image} || echo Image ${image} not found.
done
echo
echo "Checking container status..."
podman container list -a | grep ${container_name} || echo container ${container_name} not found.
echo
echo "Checking systemd unit status..."
systemctl --user status ${service_name}
