#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

# FAIL if pod is running
ensure_pod_not_running ${pod_name}

# FAIL if pod exists
ensure_pod_not_exists ${pod_name}

# FAIL if systemd unit exists
ensure_systemd_unit_not_exists ${service_name}

# remove images
for image in ${upstream_images} ; do
    podman image rm -f $(podman image list -a -q -- ${image}) || echo Image ${image} not found.
done
