#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

# FAIL if container does not exists
ensure_container_exists "${container_name}"

# FAIL if container is running
ensure_container_not_running "${container_name}"

"${ABSDIR}"/70_disable.sh

rm -f "${HOME}/.config/systemd/user/${service_name}"
systemctl --user daemon-reload
podman container rm "${container_name}"

for volume in ${nonpersistent_volumes}; do
    podman volume rm "${volume}"
done
