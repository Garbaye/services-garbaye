#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

# FAIL if container is running
ensure_container_not_running "${container_name}"

# FAIL if container exists
ensure_container_not_exists "${container_name}"

# FAIL if systemd unit exists
ensure_systemd_unit_not_exists "${service_name}"

# remove images
for image in ${upstream_images} ; do
    podman image rm -f $(podman image list -a -q -- ${image}) || echo Image ${image} not found.
done
