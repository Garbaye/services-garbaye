#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

# pod should exist
ensure_pod_exists "${pod_name}"
# pod should NOT be running. Installation script must have stopped it.
ensure_pod_not_running "${pod_name}"

ensure_systemd_as_user_dir_exists

podman generate systemd --stop-timeout=30 --files --name "${pod_name}"
mv -- *.service ~/.config/systemd/user/

systemctl --user daemon-reload
systemctl --user enable "${service_name}"
