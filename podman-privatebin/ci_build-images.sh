#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

buildfolder=/tmp/privatebin-$$

if ! podman image exists ${privatebin_image}:${privatebin_version}; then
    if git clone -b ${privatebin_version} --depth=1 https://github.com/PrivateBin/docker-nginx-fpm-alpine ${buildfolder} ; then
        arg_release=$(grep 'ARG RELEASE' ${buildfolder}/Dockerfile | cut -d= -f2)
        read type tag_sha < <(echo $(curl -s "https://api.github.com/repos/PrivateBin/PrivateBin/git/ref/tags/${arg_release}" | jq -r '.object.type,.object.sha'))
        if [ $type == "commit" ]; then
            sha=$tag_sha
        else
            sha=$(curl -s "https://api.github.com/repos/PrivateBin/PrivateBin/git/tags/$tag_sha" | jq -r '.object.sha')
        fi
	sed -i "s#mv cfg lib tpl vendor /srv#mv cfg lib tpl vendor /srv \&\& sed -i \\'/Content-Security-Policy\\\|X-XSS-Protection\\\|X-Frame-Options\\\|X-Content-Type-Options/d\\' /srv/lib/Controller.php#" ${buildfolder}/Dockerfile &&
        TMPDIR=${HOME} podman image build \
		--label "org.opencontainers.image.revision=${sha}" \
		-t "${privatebin_image}":"${privatebin_version}" \
		${buildfolder} || retval=false
	podman image prune -a -f --filter dangling=true
	podman image prune -a -f --filter intermediate=true
	podman image rm -f $(podman image list -a -q -- docker.io/library/alpine)
    fi
    rm -rf ${buildfolder}
    eval "$retval"
else
    echo "Image ${privatebin_image}:${privatebin_version} already built"
fi &&

oci_push_to_registry ${privatebin_image}:${privatebin_version}
