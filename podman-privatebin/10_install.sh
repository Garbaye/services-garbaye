#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

if ! podman volume exists ${srvdata_volume} ; then
    echo "Error : data volume ${srvdata_volume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

if ! podman image exists "${privatebin_image}":"${privatebin_version}"; then
    podman image pull "${privatebin_image}":"${privatebin_version}" || exit 1
fi

sha=$(skopeo inspect docker://"${privatebin_image}":"${privatebin_version}" | jq -r '.Labels."org.opencontainers.image.revision"')
curl -so ${HOME}/conf.php https://raw.githubusercontent.com/PrivateBin/PrivateBin/${sha}/cfg/conf.sample.php
# disable never expire option
sed_in_place "never = 0" ";never = 0" ${HOME}/conf.php

podman container run -d --read-only --name ${container_name} \
       -p ${listen_if}:${listen_port}:8080 \
       --mount=type=tmpfs,destination=/run,U=true \
       --mount=type=tmpfs,destination=/tmp \
       --mount=type=tmpfs,destination=/var/lib/nginx/tmp,U=true \
       -v ${srvdata_volume}:/srv/data:U,Z \
       -v ${HOME}/conf.php:/srv/cfg/conf.php:U,Z,ro \
       "${privatebin_image}":"${privatebin_version}" &&
( podman container logs -f ${container_name} 2>&1 & ) | grep -q 'NOTICE: ready to handle connections' &&
podman container stop ${container_name} &&
echo Container ${container_name} successfully built and stopped.
