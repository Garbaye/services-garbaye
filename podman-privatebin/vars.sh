#!/usr/bin/env bash
## vars
privatebin_image="git.garbaye.fr/garbaye/privatebin-nginx-fpm-alpine"
privatebin_version='1.7.6-alpine3.21'
## default vars : override with ENV var
listen_if="${GARBAYE_PRIVATEBIN_ENV_LISTENIF:-127.0.0.1}"
listen_port="${GARBAYE_PRIVATEBIN_ENV_LISTENPORT:-8084}"
## mandatory ENV vars
upstream_images="${privatebin_image} docker.io/library/alpine"
## internal vars : do not touch
project_name=${PWD##*/}
container_name="${project_name}"
service_name="container-${container_name}.service"
srvdata_volume='privatebin-data'
