#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_variables_are_defined "$envvars"

if podman volume exists ${dbvolume} ; then
    echo "Error : DB volume ${dbvolume} already exists."
    echo "Please remove it before a freshinstall, or continue with a standard installation."
    exit 1
fi

if podman volume exists ${datavolume} ; then
    echo "Error : DATA volume ${datavolume} already exists."
    echo "Please remove it before a freshinstall, or continue with a standard installation."
    exit 1
fi

#if podman volume exists ${uploadsvolume} ; then
#    echo "Error : UPLOADS volume ${uploadsvolume} already exists."
#    echo "Please remove it before a freshinstall, or continue with a standard installation."
#    exit 1
#fi

podman volume create ${dbvolume}
podman volume create ${datavolume} #&& podman unshare chmod 0777 `get_podman_volume_path ${datavolume}`
#podman volume create ${uploadsvolume} && podman unshare chmod 0777 `get_podman_volume_path ${uploadsvolume}`

${ABSDIR}/10_install.sh
