#!/usr/bin/env bash
## vars
#hedgedoc_image="quay.io/hedgedoc/hedgedoc"
hedgedoc_image="git.garbaye.fr/garbaye/hedgedoc"
hedgedoc_version='1.9.9-alpine'
database_image="docker.io/library/postgres"
database_version='14-alpine'
database_path="/var/lib/postgresql/data"
database_dialect=postgres
database_port=5432
#database_image="docker.io/library/mariadb"
#database_version='10'
#database_path="/var/lib/mysql"
#database_dialect=mysql
#database_port=3306
## mandatory ENV vars
envvars='GARBAYE_HEDGEDOC_DATABASE_PASSWORD GARBAYE_HEDGEDOC_DOMAIN'
## internal vars : do not touch
project_name=${PWD##*/}
pod_name="pod_${project_name}"
service_name="pod-${pod_name}.service"
upstream_images="${hedgedoc_image} ${postgres_image}"
datavolume="${project_name}_data"
dbvolume="${project_name}_database"
container_name="${project_name}_app"
db_container_name="${project_name}_database"
