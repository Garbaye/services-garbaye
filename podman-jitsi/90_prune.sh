#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

# Run regular prune script for pods
source ${ABSDIR}/../_podman-common/90_prune_pod.sh &&
# Remove configuration volume
podman unshare rm -rf ${confvolume}/
