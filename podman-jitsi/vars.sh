#!/usr/bin/env bash
## vars
version='stable-10078-1'
## default vars : override with ENV var
listen_if="${GARBAYE_JITSI_ENV_LISTENIF:-127.0.0.1}"
listen_port="${GARBAYE_JITSI_ENV_LISTENPORT:-8085}"
jicofo_port="${GARBAYE_JITSI_ENV_JICOFORESTPORT:-8093}"
colibri_port="${GARBAYE_JITSI_ENV_COLIBRIPORT:-8094}"
## mandatory ENV vars
envvars='GARBAYE_JITSI_URL'
## internal vars : do not touch
project_name=${PWD##*/}
pod_name="pod_${project_name}"
service_name="pod-${pod_name}.service"
get_default_iface_ipv4 GARBAYE_JITSI_PRIV_IP
upstream_images="git.garbaye.fr/garbaye/jitsi-jvb git.garbaye.fr/garbaye/jitsi-jicofo git.garbaye.fr/garbaye/jitsi-prosody git.garbaye.fr/garbaye/jitsi-web"
confvolume="${HOME}/.jitsi-meet-cfg"
