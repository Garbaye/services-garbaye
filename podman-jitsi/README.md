# jitsi
Créée le Sunday 23 May 2021

### **Suivre les releases - flux RSS**
<https://github.com/jitsi/docker-jitsi-meet/releases.atom>

### **Get current version**
curl -sI <https://github.com/jitsi/docker-jitsi-meet/releases/latest> | grep ^location | awk -F/ '{print $NF}'

