#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

if ! podman image exists ${ntfy_image}:${ntfy_version}; then
    TMPDIR=${HOME} podman image build -t "${ntfy_image}:${ntfy_version}" container/ --build-arg NTFY_VERSION=${ntfy_version} || retval=false
    podman image prune -a -f --filter dangling=true
    podman image prune -a -f --filter intermediate=true
    podman image rm -f $(podman image list -a -q -- docker.io/library/golang)
    podman image rm -f $(podman image list -a -q -- docker.io/library/alpine)
    eval "$retval"
else
    echo "Image ${ntfy_image}:${ntfy_version} already built"
fi &&

oci_push_to_registry ${ntfy_image}:${ntfy_version}
