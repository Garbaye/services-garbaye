#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_container_exists ${container_name}
ensure_variables_are_defined "$envvars"

upstream_version=${ntfy_version}
current_version=$(podman container inspect -f '{{.ImageName}}' ${container_name} | awk -F: '{print $NF}')
current_image=$(podman container inspect -f '{{.ImageName}}' ${container_name} | awk -F: '{print $1}')

reinstall_please () {
    if ! podman image exists ${ntfy_image}:${upstream_version}; then
	podman image pull ${ntfy_image}:${upstream_version} || exit 1
    fi &&
    check_container_running ${container_name} && ./40_stop.sh
    ./70_disable.sh && \
    ./80_destroy.sh && \
    ./10_install.sh && \
    ./20_enable.sh && \
    ./30_start.sh
}

cleanup_images () {
    echo "Remove ${current_version} images?"
    select yn in "Yes" "No"; do
	case $yn in
	    Yes)
		podman image rm ${current_image}:${current_version}
		exit 0
		;;
	    No)
		exit 0
		;;
	esac
    done
}

if [ ${current_version} != ${upstream_version} ]; then
    if [[ "${current_version}" > "${upstream_version}" ]]; then
	echo "WARNING : you are about to DOWNGRADE your installation"
    fi
    echo "Migrating from ${current_version} to ${upstream_version}. Proceed?"
    select yn in "Yes" "No"; do
	case $yn in
	    Yes)
		reinstall_please && \
		cleanup_images
		exit 0
		;;
	    No)
		exit 0
		;;
	esac
	done
else
    echo "Already using version ${ntfy_version}. Exiting."
fi
