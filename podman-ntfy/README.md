# Utilisateurs et Permissions (ACL)
Documentation pour la gestion des [utilisateurs](https://docs.ntfy.sh/config/#users-and-roles) et [permissions](https://docs.ntfy.sh/config/#access-control-list-acl).  

Pour lancer ntfy en interactif dans l'environnement `podman`, préfixer les commandes par `podman container exec -it podman-ntfy` :

```
$ podman container exec -it podman-ntfy ntfy access 
user * (anonymous)
- no topic-specific permissions
- read-write access to all (other) topics (server config)

$ podman container exec -it podman-ntfy ntfy user add didier
user didier added with role user

$ podman container exec -it podman-ntfy ntfy access didier prefix_* rw
granted read-write access to topic prefix_*

user didier (user)
- read-write access to topic prefix_*

$ podman container exec -it podman-ntfy ntfy access everyone prefix_* deny
revoked all access to topic prefix_*

user * (anonymous)
- no access to topic prefix_*
- read-write access to all (other) topics (server config)

```
