# Gitea

## Notes
Pour ajouter un utilisateur (premier compte admin ou si la création de comptes est désactivée) :
```
podman container exec --user git gitea gitea admin user create --admin --username <name> --password <password> --email <email>
```
Vérification de la bonne santé de l'instance, à faire après chaque mise à jour :
```
podman container exec --user git gitea gitea doctor check --all
```
[Config cheat sheet](https://docs.gitea.io/en-us/config-cheat-sheet/)

## Restauration
À partir d'une copie des home utilisateur (volumes podman compris) dans `/backup` :
* En tant qu'utilisateur `podman-gitea` :
```
podman volume create podman-gitea_gitea-data
podman volume create podman-gitea_gitea-pgsql
```
* En tant que `root` :
```
rsync -a /backup/home/podman-gitea/.local/share/containers/storage/volumes/podman-gitea_gitea-data/_data ~podman-gitea/.local/share/containers/storage/volumes/podman-gitea_gitea-data/
rsync -a /backup/home/podman-gitea/.local/share/containers/storage/volumes/podman-gitea_gitea-pgsql/_data ~podman-gitea/.local/share/containers/storage/volumes/podman-gitea_gitea-pgsql/

chown -R podman-gitea:podman-users ~podman-gitea/.local/share/containers/storage/volumes/podman-gitea_gitea-data/_data
chown -R podman-gitea:podman-users ~podman-gitea/.local/share/containers/storage/volumes/podman-gitea_gitea-pgsql/
```
* En tant qu'utilisateur `podman-gitea` :
```
podman unshare chown -R 999:999 ~/.local/share/containers/storage/volumes/podman-gitea_gitea-pgsql/_data
podman unshare chown -R 1000:1000 ~/.local/share/containers/storage/volumes/podman-gitea_gitea-data/_data/git
podman unshare chown -R 1000:1000 ~/.local/share/containers/storage/volumes/podman-gitea_gitea-data/_data/gitea
```
* Exporter les variables d'environnement et procéder à l'installation normale (10_install.sh)
