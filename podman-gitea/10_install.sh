#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_pod_not_exists ${pod_name}
ensure_variables_are_defined "$envvars"

if ! podman volume exists ${dbvolume} ; then
    echo "Error : DB volume ${dbvolume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

if ! podman volume exists ${datavolume} ; then
    echo "Error : DATA volume ${datavolume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

cat <<EOT >> .env
APP_NAME=${GARBAYE_GITEA_APPNAME}
GITEA__database__DB_TYPE=postgres
GITEA__database__HOST=db:5432
GITEA__database__NAME=gitea
GITEA__database__PASSWD=${GARBAYE_GITEA_POSTGRES_PASSWORD}
GITEA__database__USER=gitea
GITEA__mailer__ENABLED=true
GITEA__mailer__FROM=${GARBAYE_GITEA_SMTP_FROM}
GITEA__mailer__SMTP_ADDR=${GARBAYE_GITEA_SMTP_ADDR}
GITEA__mailer__SMTP_PORT=25
GITEA__security__INSTALL_LOCK=true
GITEA__server__DOMAIN=${GARBAYE_GITEA_DOMAIN}
GITEA__server__SSH_DOMAIN=${GARBAYE_GITEA_DOMAIN}
GITEA__server__ROOT_URL="https://${GARBAYE_GITEA_DOMAIN}"
GITEA__server__SSH_PORT=${sshlisten_port}
GITEA__service__DISABLE_REGISTRATION=true
POSTGRES_DB=gitea
POSTGRES_PASSWORD=${GARBAYE_GITEA_POSTGRES_PASSWORD}
POSTGRES_USER=gitea
EOT

export gitea_image
export gitea_version
export postgres_image
export postgres_version
export weblisten_if
export weblisten_port
export sshlisten_if
export sshlisten_port

if ! podman image exists ${gitea_image}:${gitea_version}; then
    podman image pull ${gitea_image}:${gitea_version} || exit 1
fi
podman image pull ${postgres_image}:${postgres_version} &&
# force refresh of app.ini
podman unshare rm -f `get_podman_volume_path ${datavolume}`/gitea/conf/app.ini
${my_podman_compose} --pod-args="--infra=true --infra-name=${project_name}_infra --share=" --podman-run-args "--requires=${project_name}_infra --env-file .env" up -d &&
echo -n "Waiting for gitea to finish starting " &&
( podman container logs -f ${container_name} 2>&1 & ) | grep -q 'NewServer()' &&
echo "OK" &&
podman unshare install -d -o 1000 -g 1000 `get_podman_volume_path ${datavolume}`/gitea/public/ &&
curl -o robots.txt https://code.forgejo.org/robots.txt &&
podman unshare install -o 1000 -g 1000 robots.txt `get_podman_volume_path ${datavolume}`/gitea/public/robots.txt &&
podman container exec --user 1000:1000 gitea gitea embedded extract --overwrite --destination /data/gitea/ 'templates/home.tmpl' &&
podman unshare sed -i '/{{ctx.Locale.Tr \"startpage.app_desc\"}}/,${s//Plateforme collaborative de projets libres./;b};$q1' `get_podman_volume_path ${datavolume}`/gitea/templates/home.tmpl &&
podman unshare sed -i '/{{ctx.Locale.Tr \"startpage.install\"}}/,${s//Qui peut avoir un compte ?/;b};$q1' `get_podman_volume_path ${datavolume}`/gitea/templates/home.tmpl &&
podman unshare sed -i '/{{ctx.Locale.Tr \"startpage.install_desc\"}}/,${s//Tous ceux qui partagent de près ou de loin nos idées ou notre vision./;b};$q1' `get_podman_volume_path ${datavolume}`/gitea/templates/home.tmpl &&
podman unshare sed -i '/{{ctx.Locale.Tr \"startpage.lightweight\"}}/,${s//Comment créer un compte ?/;b};$q1' `get_podman_volume_path ${datavolume}`/gitea/templates/home.tmpl &&
podman unshare sed -i '/{{ctx.Locale.Tr \"startpage.lightweight_desc\"}}/,${s//Pour des raisons de capacité et de gestion, l\x27enregistrement de compte en ligne est désactivé. Si vous souhaitez avoir un compte, <a href="https:\/\/garbaye.fr\/contact\/">contactez nous<\/a> pour nous parler de vos projets et besoins./;b};$q1' `get_podman_volume_path ${datavolume}`/gitea/templates/home.tmpl &&
podman unshare sed -i '/{{ctx.Locale.Tr \"startpage.platform\"}}/,${s//Quels contenus sont acceptés ?/;b};$q1' `get_podman_volume_path ${datavolume}`/gitea/templates/home.tmpl &&
podman unshare sed -i '/{{ctx.Locale.Tr \"startpage.platform_desc\"}}/,${s//La forge accueille tout type de contenu tel que code source, documentation, artistique, littéraire, etc. Un usage raisonnable en espace disque est attendu de nos utilisateurs./;b};$q1' `get_podman_volume_path ${datavolume}`/gitea/templates/home.tmpl &&
podman unshare sed -i '/{{ctx.Locale.Tr \"startpage.license\"}}/,${s//Licences libres/;b};$q1' `get_podman_volume_path ${datavolume}`/gitea/templates/home.tmpl &&
podman unshare sed -i '/{{ctx.Locale.Tr \"startpage.license_desc\"}}/,${s//Tous les projets présents sur cette forge sont publiés sous licences libres, <a href="https:\/\/www.gnu.org\/licenses\/license-list.fr.html">au sens de la Free Software Foundation<\/a>./;b};$q1' `get_podman_volume_path ${datavolume}`/gitea/templates/home.tmpl &&
bats test/ &&
podman pod stop ${pod_name} &&
echo Pod built and stopped. &&
shred -u .env
