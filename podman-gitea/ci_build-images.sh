#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

buildfolder=/tmp/gitea-$$

if ! podman image exists ${gitea_image}:${gitea_version}; then
    mkdir ${buildfolder} &&
    if git clone -b v${gitea_version} --depth=1 https://codeberg.org/forgejo/forgejo/ ${buildfolder} ; then
	TMPDIR=${HOME} podman image build \
	               --label org.opencontainers.image.version="${gitea_version}" \
		       -t ${gitea_image}:${gitea_version} \
		       -f ${buildfolder}/Dockerfile \
		       ${buildfolder} || retval=false
	podman image prune -a -f --filter dangling=true
	podman image prune -a -f --filter intermediate=true
	podman image rm -f $(podman image list -a -q -- docker.io/library/golang)
	podman image rm -f $(podman image list -a -q -- docker.io/library/alpine)
	podman image rm -f $(podman image list -a -q -- docker.io/tonistiigi/xx)
    fi
    rm -rf ${buildfolder}
    eval "$retval"
else
    echo "Image ${gitea_image}:${gitea_version} already built"
fi &&

oci_push_to_registry ${gitea_image}:${gitea_version}
