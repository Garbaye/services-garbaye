#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

buildfolder=/tmp/synapse-$$

if ! podman image exists ${synapse_image}:${synapse_version}; then
    mkdir ${buildfolder} &&
    if git clone -b ${synapse_version} --depth=1 https://github.com/element-hq/synapse/ ${buildfolder} ; then
        rm -rf "${HOME}/buildah-cache-${UID}"
	TMPDIR=${HOME} podman image build \
	      -t ${synapse_image}:${synapse_version} \
	      -f ${buildfolder}/docker/Dockerfile \
	      ${buildfolder}/ || retval=false
	podman image prune -a -f --filter dangling=true
	podman image prune -a -f --filter intermediate=true
	podman image rm -f $(podman image list -a -q -- docker.io/library/python)
    fi
    rm -rf ${buildfolder}
    eval "$retval"
else
    echo "Image ${synapse_image}:${synapse_version} already built"
fi &&

oci_push_to_registry ${synapse_image}:${synapse_version}
