#!/usr/bin/env bash
## vars
synapse_image="git.garbaye.fr/garbaye/matrixdotorg-synapse"
synapse_version='v1.125.0'
postgres_image="docker.io/library/postgres"
postgres_version='16-alpine'
## default vars : override with ENV var
listen_if="${GARBAYE_MATRIX_ENV_LISTENIF:-127.0.0.1}"
listen_port="${GARBAYE_MATRIX_ENV_LISTENPORT:-8086}"
## mandatory ENV vars
envvars='GARBAYE_MATRIX_POSTGRES_PASSWORD GARBAYE_MATRIX_DOMAIN GARBAYE_MATRIX_URL'
## internal vars : do not touch
project_name=${PWD##*/}
pod_name="pod_${project_name}"
service_name="pod-${pod_name}.service"
upstream_images="${synapse_image} ${postgres_image}"
confvolume='podman-matrix_matrixdotorg_synapse-data'
dbvolume='podman-matrix_matrixdotorg_synapse-pgsql'
container_name='synapse'
db_container_name='synapse-db'
