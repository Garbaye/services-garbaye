# Matrix (Synapse)
## Créer un utilisateur après le ./30_start.sh
``` bash
podman container exec -ti synapse register_new_matrix_user -c /data/homeserver.yaml http://localhost:8008
```
## Accéder à l'administration de l'instance pour gérer les utilisateurs, salons, medias...
``` bash
podman container run --rm --rmi -p 8080:80 docker://awesometechnologies/synapse-admin
```
Se connecter ensuite à l'interface web avec un compte disposant des permissions "admistrateur serveur" sur http://localhost:8080

## Reste à faire:
- gérer les UID/GID et comptes utilisés dans les containers ?
- Password policy ?
- Fédération+redis l 2831
## Restauration
À partir d'une copie des home utilisateur (volumes podman compris) dans `/backup` :
* En tant qu'utilisateur `podman-matrix` :
```
podman volume create matrixdotorg_synapse-data
podman volume create matrixdotorg_synapse-pgsql
```
* En tant que `root` :
```
rsync -a /backup/home/podman-matrix/.local/share/containers/storage/volumes/matrixdotorg_synapse-data/_data ~podman-matrix/.local/share/containers/storage/volumes/matrixdotorg_synapse-data/
rsync -a /backup/home/podman-matrix/.local/share/containers/storage/volumes/matrixdotorg_synapse-pgsql/_data ~podman-matrix/.local/share/containers/storage/volumes/matrixdotorg_synapse-pgsql/
chown -R podman-matrix:podman-users ~podman-matrix/.local/share/containers/storage/volumes/matrixdotorg_synapse-data/_data
chown -R podman-matrix:podman-users ~podman-matrix/.local/share/containers/storage/volumes/matrixdotorg_synapse-pgsql/_data
```
* En tant qu'utilisateur `podman-matrix` :
```
podman unshare chown -R 999:999 ~/.local/share/containers/storage/volumes/matrixdotorg_synapse-pgsql/_data
podman unshare chown -R 991:991 ~/.local/share/containers/storage/volumes/matrixdotorg_synapse-data/_data
```
* Exporter les variables d'environnement et procéder à l'installation normale (10_install.sh)
