#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

buildfolder=/tmp/vaultwarden-$$

if ! podman image exists ${service_image}:${service_version}; then
    mkdir ${buildfolder} &&
    if git clone --depth=1 --branch=${service_version%%-*} https://github.com/dani-garcia/vaultwarden.git ${buildfolder}/ ; then
        vaultwardenlangfrfolder=/tmp/vaultwardenlangfrfolder-$$
        if git clone --depth=1 https://github.com/YoanSimco/vaultwarden-lang-fr.git ${vaultwardenlangfrfolder}/; then
	    install -v ${vaultwardenlangfrfolder}/email/* ${buildfolder}/src/static/templates/email/
	fi
	rm -rf ${vaultwardenlangfrfolder}
	TMPDIR=${HOME} DB='sqlite' CONTAINER_REGISTRIES='git.garbaye.fr/garbaye/vaultwarden' BASE_TAGS=${service_version%%-*} ${buildfolder}/docker/podman-bake.sh || retval=false
	podman image prune -a -f --filter dangling=true
	podman image prune -a -f --filter intermediate=true
	podman image rm -f $(podman image list -a -q -- docker.io/vaultwarden/web-vault)
	podman image rm -f $(podman image list -a -q -- docker.io/library/rust)
	podman image rm -f $(podman image list -a -q -- docker.io/library/debian)
	podman image rm -f $(podman image list -a -q -- docker.io/tonistiigi/xx)
    fi
    rm -rf ${buildfolder}
    eval "$retval"
else
    echo "Image ${service_image}:${service_version} already built"
fi &&

oci_push_to_registry "${service_image}":"${service_version}"
